"""
This is about formatting the Strings in Python using different methods -
    1. Old format % Modulo Operator
    2. Using "{}.format()" method
    3. String Interpolation "f'"
    4. String Template (User Defined Template)
"""
from string import Template

# User inputs for the patient details ...
fancyStrings = "*"
patientDetails = "PATIENT DETAILS"
patientFirstName = input("\nEnter the Patient's FIRST NAME: ")
patientFirstName = patientFirstName.upper()
patientLastName = input("Enter the patient's LAST NAME: ")
patientLastName = patientLastName.upper()
patientID = int(input("Enter the Patient's Medical Record Number: "))
patientHeight = float(input("Enter the Patient's HEIGHT (in m): "))
patientWeight = float(input("Enter the Patient's WEIGHT (in kg): "))

print("\n")
print((fancyStrings * 5) + " " + patientDetails + " " + (fancyStrings * 5))

# 1. String Formation using "OLD" % MODULO OPERATOR -
# patientDetails = (
#     "\n First Name : %s"
#     "\n Last Name  : %s"
#     "\n MRN        : %d"
#     "\n Height     : %.2f meters"
#     "\n Weight     : %.2f kgs" % (patientFirstName, patientLastName, patientID, patientHeight, patientWeight)
# )
# print(patientDetails)

# 2. String Formation using {}.format() method -
# print(
#     "First Name :{patFirstName}\n"
#     "Last Name  :{patLastName}\n"
#     "MRN        :{patID}\n"
#     "Height     :{patHeight} meters\n"
#     "Weight     :{patWeight} kgs".format(patWeight=patientWeight, patID=patientID, patHeight=patientHeight,
#                                          patLastName=patientLastName, patFirstName=patientFirstName)
# )

# 3. String formation using f' INTERPOLATION STRING
patientDetails = (
    f'First Name: {patientFirstName}'
    f'\nLast Name : {patientLastName}'
    f'\nMRN       : {patientID}'
    f'\nHeight    : {patientHeight} meters'
    f'\nWeight    : {patientWeight} kgs'
)
print(patientDetails)
