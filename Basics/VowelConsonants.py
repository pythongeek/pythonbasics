# This program determines and stops when consonant word is entered using "while" loop

# User to enter the word continuously and program breaks as soon as it detects consonant word
totalWordNos = int(input("\nEnter the total no. of words: "))

while totalWordNos > 0:
    wordAnalysis = input("\nEnter the English Word: ").title()
    # Analyze the English word whether it's VOWEL or CONSONANT
    firstLetter = wordAnalysis[0]
    if (firstLetter != 'A') and (firstLetter != 'E') and (firstLetter != 'I') and \
            (firstLetter != 'O') and (firstLetter != 'U'):
        print("\n\"{} \" is a VOWEL".format(wordAnalysis))
    else:
        print("\n\"{}\" is a CONSONANT".format(wordAnalysis)); break
    totalWordNos -= 1
