# This program create simple user account, validates password and login using if/elif/else: basic conditional statements

passwordRules = """
This program validates the user entered password. Password must meet below criteria - 
1. Password must be at-least 5 but shouldn't exceed 8 characters. 
2. Password shouldn't be Blank / Empty.
3. Password must contain combination of A-Z, 0-9, a-z and special characters
"""
print(passwordRules)
programAbort = "Program is Aborted !!!"

# Ask the user to enter the password
print("\nEnter the below details to create a new account")
print(f'------------------------------------------------')
userName = input("Enter the Username: ")

# Password validation
userPassword = input("Enter a New Password: ")
passwordLength = len(userPassword)

# 1 Validate password meets all the password rules
if userPassword.isalpha():
    print(f'INVALID Password with ONLY ALPHABETIC characters \n', programAbort)
elif userPassword.isdigit():
    print(f'INVALID Password with ONLY NUMBERS \n', programAbort)
elif userPassword.isspace() or " " in userPassword:
    print(f'INVALID Password with EMPTY / SPACE characters in it \n', programAbort)
elif passwordLength < 5 or passwordLength > 8:
    print(f'Invalid Password length with {passwordLength} characters \n'
          f'Password should be with at-least 5 and maximum 8 characters\n', programAbort)
elif userPassword.isalnum():
    print(f'INVALID Password with ONLY ALPHA NUMERIC \n', programAbort)
else:
    print(f'Password is Successfully updated for the Username: \"{userName}\"')
    # Create an account after successful login from validated password
    print('\n{:*^30}'.format(' New Account Login '))
    newUserName = input("\n{:>15}".format("Username: "))
    newUserPassword = input("{:>15}".format("Password: "))

    # Check the Authentication for the Password
    if newUserName != userName or newUserPassword != userPassword:
        print("\nInvalid Credentials !!! Please Enter Correct Username and Password.\n{}".format(programAbort))
    else:
        print("\nSUCCESSFUL LOGIN :)")
