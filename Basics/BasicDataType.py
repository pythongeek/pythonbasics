""" This is the file with Basic Data Types in Python """

# This is an INTEGER Number ...
intNumber = 30
print("This is an Integer Number: ")
print(intNumber)

# This is a FLOATING Number ...
floatNumber = 90000000000000.0
print("\n\nThis is aFloating Number: ")
print(floatNumber)

# This is a Python STRING ...
# Single quoted string  with escaping character -
print('\n\nWelcome to Python Programming.')
print('\nHey How\'re you doing today?')

# Double quoted string with escape character -
print("\nHappy Pythoning !!!")
print("\n\"PYTHON\" is one of the Best Programming Language")

# Triple quoted string  -
print('''Hey, you've started with 
Python Programing Language. 
''')