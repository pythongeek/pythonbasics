print("""
This program determines the PATIENT'S AGE PROFILE based on entered Birth Year -
    1. Age 1 - 2: Infant / Newborn Baby
    2. Age 3 - 7: Paediatric
    3. Age 8 - 18: Teenage
    4. Age 19 - 60: Adult
    5. Age 60 - 150: Senior Citizen
    6. Invalid Age Profile outside 1 - 150 Years.
""")

CURRENT_YEAR = 2020
# User Input for the Patient's Birth Year
patientBirthYear = int(input("\nEnter the Patient's BIRTH YEAR (YYYY): "))

# Calculate current Age of the patient
currentPatientAge = CURRENT_YEAR - patientBirthYear

# Calculates AGE PROFILE based on the Birth Year using if-elif-else conditional statement
if currentPatientAge >= 1 and currentPatientAge < 3:
    print(f"Patient is INFANT / NEWBORN BABY with {currentPatientAge} years")
elif currentPatientAge >= 3 and currentPatientAge <= 7:
    print(f"Patient is PAEDIATRIC with {currentPatientAge} years")
elif currentPatientAge >= 8 and currentPatientAge <= 18:
    print(f"Patient is TEENAGE with {currentPatientAge} years")
elif currentPatientAge >= 19 and currentPatientAge <= 60:
    print(f"Patient is ADULT with {currentPatientAge} years")
elif currentPatientAge >= 61 and currentPatientAge <= 150:
    print(f"Patient is SENIOR CITIZEN with {currentPatientAge} years")
else:
    print(f"Patient with {currentPatientAge} years is INVALID AGE GROUP !!!"
          f"\nPlease make sure the AGE GROUP is with <1 - 150> Years ONLY.")
