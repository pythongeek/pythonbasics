"""
This program provide the cost for each purchased item and calculate the total amount and provide the Bill
This uses the concept of Dictionary and List
"""

# Define the ITEMS LIST and COST DETAILS
itemStocks = []; itemCost = []
totalItems = totalCost = 0

print("\n{:*^50}".format(" Welcome to ONLINE MEGA MART "))
# Ask the user to enter no. of items and its respective cost
itemQuantity = int(input(f'\nEnter the No.of ITEMS to be purchased: '))

for item in range(itemQuantity):
    itemName = str(input(f'\nEnter the {item+1}. ITEM NAME: ')).upper()
    itemStocks.append(itemName)
    itemPrice = float(input(f'Enter the {itemName}\'s PRICE (Rs.): '))
    totalCost += itemPrice
    itemCost.append(str(itemPrice))

print(f'{itemStocks}')
print(f'{itemCost}')

# Convert the list of ITEMS and its COST to DICTIONARY and display the details
convertedItemList = zip(itemStocks, itemCost)
purchasedItemDetails = dict(convertedItemList)
print(purchasedItemDetails)

# Display the ITEMS and COST details
print("\n Total Purchased ITEMS and COST Details")
for itemName in purchasedItemDetails:
    print(f'{itemName}  : {purchasedItemDetails[itemName]}')

print("\nTotal ITEMS purchased: {}".format(len(itemStocks)))
print("Total COST of {} ITEMS: Rs. {}".format(len(itemStocks), totalCost))
