"""
This Program Configures the Alarm Limits, Priority for selected Physiological Alarms
And, display the Alarms with Priority and Alarm Limits as configured.
"""
from time import sleep

physiologicalParameters = ['SPO2', 'PR', 'RR']

# Colored output format
notificationColor = "\33[31m"; notificationPlain = "\33[0m"; notificationFlash = "\33[6m"
ALARM_PRIORITY = {
    'HIGH': "\33[91m",
    'MEDIUM': "\33[33m",
    'LOW': "\33[34m",
    'INFO': "\33[7m",
}

# Define a blank Dictionary for PARAMETERS
vitalSigns = {}
parameterLimit = {}
paramHL = 'High'
paramLL = 'Low'

# Select the parameter for different configurations -
while True:
    print("\n {:*^40}".format(" Physiological Parameters "))
    for param in range(len(physiologicalParameters)):
        print("{}.{}".format(param + 1, physiologicalParameters[param]).upper())
    selectParameter = str(input("\nSelect any one of the Parameter from the list: ")).upper()

    # Configure Alarm Limits if parameter is existing, else ask the user to enter appropriate parameter from list
    if selectParameter in physiologicalParameters:
        while True:
            paramLowLimit = str(input(f'\nConfigure the {selectParameter} LOW LIMIT: '))
            paramHighLimit = str(input(f'Configure the {selectParameter} HIGH LIMIT: '))
            if not (paramLowLimit.isdigit() and paramHighLimit.isdigit()):
                print(f'\nEnter ONLY DIGITS for LOW/HIGH LIMITS!')
            elif int(paramHighLimit) < int(paramLowLimit):
                print(f'\n{notificationColor}Parameter HIGH LIMIT CAN NOT BE LESSER LOW LIMIT {notificationPlain}')
            else:
                vitalSigns[selectParameter] = dict([(paramLL, int(paramLowLimit)), (paramHL, int(paramHighLimit))])
                print(vitalSigns)
                print(f'\nSelected Parameter is: {selectParameter}'
                      f'\n{selectParameter} Low Limit: {vitalSigns[selectParameter][paramLL]} '
                      f'\n{selectParameter} High Limit: {vitalSigns[selectParameter][paramHL]}')
                break

        # Configure the Alarm Priorities
        while True:
            alarmPriority = str(input("\nSelect the {} Alarm Priority (HIGH/MEDIUM/LOW/INFO):".format(selectParameter)))
            alarmPriority = alarmPriority.upper()
            alarmDelay = int(input("Select the {} Alarm Delay (secs): ".format(selectParameter)))
            if alarmPriority in ALARM_PRIORITY.keys():
                sleep(alarmDelay)
                print(notificationFlash + ALARM_PRIORITY[alarmPriority] + selectParameter + " " + alarmPriority +
                      notificationPlain)
                break
            else:
                print(f'\"{alarmPriority}\" IS NOT CORRECT ALARM PRIORITY. SELECT THE CORRECT ONE')
    else:
        print("Selected \'{}\' IS NOT IN THE LIST".format(selectParameter))
    break
