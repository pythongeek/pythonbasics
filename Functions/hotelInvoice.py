"""
This Program prints the invoice for the food items purchased by the customers
"""
import datetime

FANCY_LINE = "-" * 50

# Enter the Hotel Name, Place and Pincode of the Place
invoiceHeaderDetails = []
print(f'\nEnter the following details to generate the invoice - \n')
HOTEL_NAME = input("HOTEL NAME: ").upper();
invoiceHeaderDetails.append(HOTEL_NAME)
HOTEL_PLACE = input("PLACE: ").upper();
invoiceHeaderDetails.append(HOTEL_PLACE)
PLACE_PINCODE = int(input("PLACE PINCODE: "));
invoiceHeaderDetails.append(PLACE_PINCODE)

currentDateTime = datetime.datetime.now()
currentDate = currentDateTime.strftime("%d-%B-%y")
currentTime = currentDateTime.strftime("%H:%M:%S")
currentDay = currentDateTime.strftime("%A")


# Display the Hotel Name, Place and Pincode at the Invoice Header
def displayInvoiceHeader(*headerDetails):
    for details in headerDetails:
        if headerDetails.index(details) == 0:
            print("\n{:*^50}".format(details))
        else:
            print("{:^50}".format(details))


# Display the current date, time and day at the Invoice Header
def displayDayTimeDate(date, time, day):
    print("\n{}, {}".format(date, time))
    print("{}".format(day))
    print(FANCY_LINE)


def main():
    displayInvoiceHeader(*invoiceHeaderDetails)  # *variable argument function
    displayDayTimeDate(currentDate, currentTime, currentDay)


if __name__ == '__main__':
    main()
