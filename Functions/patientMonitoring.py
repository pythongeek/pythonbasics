"""
This program basically performs the below operation and details out the patient condition -
1. Pre-defined and Default Patient Age Profile ### COMPLETED ###
2. Admit a new patient and Profile Identity ### COMPLETED ###
3. Patient Profile Configurations based on the Age Profile ### COMPLETED ###
4. User configurations for Alarm Limits, Priorities and Delay
5. Patient condition and alerts
"""
import datetime

currentYear = datetime.datetime.now()
currentYear = currentYear.year

fancyLine = "----------------------------------------------------------"
programHeader = " PATIENT MONITORING "
print('\n\33[7m {:*^50}'.format(programHeader) + "\33[0m")

ALARM_PRIORITY_1 = "\33[91m" + "High" + "\33[0m"
ALARM_PRIORITY_2 = "\33[33m" + "Medium" + "\33[0m"
ALARM_PRIORITY_3 = "\33[34m" + "Low" + "\33[0m"
ALARM_PRIORITY_4 = "\33[2m" + "Informational" + "\33[0m"

parameterList = ['SPO2', 'PR', 'RR']


# Patient Admission Details
def admitPatientDetails():
    print("\nADMIT A PATIENT TO START THE MONITORING ... ")
    firstName = input("\nEnter the Patient's FIRST NAME: ")
    lastName = input("Enter the Patient's LAST NAME: ")
    patientName = (firstName + "," + lastName)
    return patientName


# Pre-defined DEFAULT Profile Configurations for Parameters Alarm Limits
def parameterDefaultLimits(ageProfile, spo2LL, spo2HL, prLL, prHL, rrLL, rrHL):
    defaultAlarmLimits = {
        "physioAlarmLimits": {
            'SPO2': {'Low': spo2LL, 'High': spo2HL},
            'PR': {'Low': prLL, 'High': prHL},
            'RR': {'Low': rrLL, 'High': rrHL}}
    }
    print(f'\nThe DEFAULT ALARM LIMITS for {ageProfile} Patient Profile is:'
          f'\n{fancyLine}'
          f'\nSpO2 Low Limit: {defaultAlarmLimits["physioAlarmLimits"]["SPO2"]["Low"]}%,'
          f'\tSpO2 High Limit: {defaultAlarmLimits["physioAlarmLimits"]["SPO2"]["High"]}%'
          f'\nPR Low Limit: {defaultAlarmLimits["physioAlarmLimits"]["PR"]["Low"]} /min, '
          f'\tPR High Limit: {defaultAlarmLimits["physioAlarmLimits"]["PR"]["High"]} /min'
          f'\nRR Low Limit: {defaultAlarmLimits["physioAlarmLimits"]["RR"]["Low"]} /min, '
          f'\tRR High Limit: {defaultAlarmLimits["physioAlarmLimits"]["RR"]["High"]} /min')


# DEFAULT Profile Configurations for Parameters Alarm Priorities
def alarmDefaultPriorities(ageProfile, ap1, ap2, ap3, ap4, ap5, ap6):
    defaultAlarmPriorities = {
        "alarmPriority": {
            'SPO2': {'low': ap1, 'high': ap2},
            'PR': {'low': ap3, 'high': ap4},
            'RR': {'low': ap5, 'high': ap6}}
    }
    print(f'\nThe DEFAULT ALARM PRIORITIES for {ageProfile} Patient Profile is: '
          f'\n{fancyLine}'
          f'\nSpO2 low: {defaultAlarmPriorities["alarmPriority"]["SPO2"]["low"]}, '
          f'\tSpO2 high: {defaultAlarmPriorities["alarmPriority"]["SPO2"]["high"]}'
          f'\nPR low: {defaultAlarmPriorities["alarmPriority"]["PR"]["low"]}, '
          f'\tPR high: {defaultAlarmPriorities["alarmPriority"]["PR"]["high"]}'
          f'\nRR low {defaultAlarmPriorities["alarmPriority"]["RR"]["low"]}, '
          f'\tRR high: {defaultAlarmPriorities["alarmPriority"]["RR"]["high"]}')


# DEFAULT Patient Age Profile Configurations for Parameters Alarm Limits and Alarm Priorities (using DICTIONARY)
def patientAgeProfile(age, patientName):
    print(fancyLine)
    print(f"Patient's Name- {patientName.upper()}")

    if age >= 1 and age <= 3:
        ageProfile = "INFANT"
        print(f"Patient's Age Profile is: {ageProfile} with {age} Years")
        print(fancyLine)

        # Print the default alarm limits ....
        parameterDefaultLimits(ageProfile, 30, 99, 30, 300, 3, 90)

        # Print the default alarm priorities ....
        alarmDefaultPriorities(ageProfile, ALARM_PRIORITY_1, ALARM_PRIORITY_2, ALARM_PRIORITY_3,
                               ALARM_PRIORITY_3, ALARM_PRIORITY_3, ALARM_PRIORITY_2)
    elif age >= 4 and age <= 10:
        ageProfile = "PAEDIATRIC"
        print(f"Patient's Age Profile is: {ageProfile} with {age} Years")
        print(fancyLine)

        # Print the default alarm limits ....
        parameterDefaultLimits(ageProfile, 40, 100, 35, 280, 5, 85)

        # Print the default alarm priorities ....
        alarmDefaultPriorities(ageProfile, ALARM_PRIORITY_3, ALARM_PRIORITY_1, ALARM_PRIORITY_1,
                               ALARM_PRIORITY_2, ALARM_PRIORITY_1, ALARM_PRIORITY_4)
    elif age >= 11 and age <= 18:
        ageProfile = "TEENAGE"
        print(f"Patient's Age Profile is: {ageProfile} with {age} Years")
        print(fancyLine)

        # Print the default alarm limits ....
        parameterDefaultLimits(ageProfile, 35, 98, 40, 250, 3, 80)

        # Print the default alarm priorities ....
        alarmDefaultPriorities(ageProfile, ALARM_PRIORITY_1, ALARM_PRIORITY_4, ALARM_PRIORITY_2,
                               ALARM_PRIORITY_3, ALARM_PRIORITY_1, ALARM_PRIORITY_3)
    elif age >= 19 and age <= 58:
        ageProfile = "ADULT"
        print(f"Patient's Age Profile is: {ageProfile} with {age} Years")
        print(fancyLine)

        # Print the default alarm limits ....
        parameterDefaultLimits(ageProfile, 40, 100, 45, 300, 5, 70)

        # Print the default alarm priorities ....
        alarmDefaultPriorities(ageProfile, ALARM_PRIORITY_2, ALARM_PRIORITY_4, ALARM_PRIORITY_1,
                               ALARM_PRIORITY_4, ALARM_PRIORITY_1, ALARM_PRIORITY_4)
    elif age >= 59 and age <= 120:
        ageProfile = "SENIOR CITIZEN"
        print(f"Patient's Age Profile is: {ageProfile} with {age} Years")
        print(fancyLine)

        # Print the default alarm limits ....
        parameterDefaultLimits(ageProfile, 50, 99, 55, 220, 4, 95)

        # Print the default alarm priorities ....
        alarmDefaultPriorities(ageProfile, ALARM_PRIORITY_1, ALARM_PRIORITY_1, ALARM_PRIORITY_2,
                               ALARM_PRIORITY_2, ALARM_PRIORITY_3, ALARM_PRIORITY_3)
    else:
        pass


# Calculate the Patient Age based on entered Birth Year
def calculatePatientAge(birthYear):
    patientAge = currentYear - birthYear
    return patientAge


# Birth year validation
def validateBirthYear(patientName):
    while True:
        birthYear = str(input("\n\33[91m" + "*" + "\33[0m" + " Enter the Patient's BIRTH YEAR: "))
        if (not birthYear.isdigit()) or birthYear.isspace() or len(birthYear) > 4 or len(birthYear) < 4:
            print("INCORRECT Birth Year !!!\n")
        elif int(birthYear) < 1900:
            print("Birth Year CAN NOT be lesser than Year: 1900 !!!\n")
        elif int(birthYear) > currentYear:
            print(f"Birth Year CAN NOT be Future than CURRENT Year: {currentYear} !!!\n")
        elif int(birthYear) == currentYear:
            print(f"Patient's Birth Year {birthYear} is SAME AS CURRENT YEAR {currentYear}:\n"
                  "Patient MUST BE at-least AN YEAR OLD to get Admit !!!")
        else:
            age = calculatePatientAge(int(birthYear))
            patientAgeProfile(age, patientName)
            break


# User confirmation to check if user likes to change DEFAULT ALARM LIMITS
def confirmUserConfigurations():
    userInput = str(input("\nWould you like to use the above same DEFAULT ALARM LIMITS for admitted patient (Yes / No)?")).upper()
    if userInput == "YES": print("The above same DEFAULT ALARM LIMITS are applied to the patient.")
    else: configureAndValidateParameterAlarmLimits()


# Parameter Alarm Limits configurations based on user inputs
def configureAndValidateParameterAlarmLimits():
    SPO2_PARAM_UNIT = "%"; PR_RR_PARAM_UNIT = "/min"

    while True:
        parameter = str(input("\nEnter the Parameter to configure (SpO2 / PR / RR): ")).upper()
        if parameter not in parameterList:
            print(f"\n\"{parameter}\" IS NOT a valid PARAMETER")
        else:
            while True:
                parameterLowLimit = str(input(f"\nEnter {parameter} Low Limit: "))
                parameterHighLimit = str(input(f"Enter {parameter} High Limit: "))
                if (not parameterLowLimit.isnumeric()) or (not parameterHighLimit.isnumeric()):
                    print(f"INVALID {parameter} Alarm Limits !!! ")
                elif int(parameterHighLimit) <= int(parameterLowLimit):
                    print(f"{parameter} Alarm High Limit: {parameterHighLimit} CAN NOT be lower than or EQUAL to its Low Limit: {parameterLowLimit}")
                else:
                    print(f"\nThe new {parameter} Parameter Alarm Limits are configured")
                    print(fancyLine)
                    if parameter == "SPO2":
                        print(f"\n{parameter} Low Limit: {parameterLowLimit} {SPO2_PARAM_UNIT}")
                        print(f"{parameter} High Limit: {parameterHighLimit} {SPO2_PARAM_UNIT}")
                    else:
                        print(f"\n{parameter} Low Limit: {parameterLowLimit} {PR_RR_PARAM_UNIT}")
                        print(f"{parameter} High Limit: {parameterHighLimit} {PR_RR_PARAM_UNIT}")
                    break
            break


def main():
    patientName = admitPatientDetails()
    validateBirthYear(patientName)
    confirmUserConfigurations()


if __name__ == '__main__':
    main()