"""
This program creates a simple Employee Data Base and allows to perform operations based on below defined USERS & ROLES -
1. SUPER ADMIN: Permission to Create, Update, Delete, Show the Employee List operations
2. EMPLOYEE: Permission to Create and Show the Employee List ONLY.
"""

print("\n{:*^80}".format(" Employee Data Base "))

# Define the user roles using PYTHON LIST
authenticatedRoles = ['Creation', 'Search', 'Updation', 'Deletion', 'Show Employee List']

# Select and authorize the user for different roles
authorizedUsers = ['Super Admin', 'Employee']

# Employee details with List database -
employeeList = []
maxEmployeeList = 5

while True:
    userSelection = str(input(f"\n1. Super Admin \n2. Employee \nSelect the User: "))
    if (not userSelection.isdigit()) or int(userSelection) > len(authorizedUsers) or int(userSelection) == 0:
        print(f'\nINCORRECT User Input !!! Select the Correct User Options from the List')
    else:
        print(f'\nSelected and Logged in SUCCESSFULLY as \"{authorizedUsers[int(userSelection) - 1]}\" USER - ')
        if userSelection == '1':
            print("\n{} can perform below operations".format(authorizedUsers[int(userSelection) - 1]))
            for roles in range(len(authenticatedRoles)):
                print("{}. {}".format(roles + 1, authenticatedRoles[roles].upper()))
        else:
            print("\n{} can perform below operations".format(authorizedUsers[int(userSelection) - 1]))
            for roles in range(len(authenticatedRoles)):
                if roles == 1 or roles == 2 or roles == 3: continue
                print("{}. {}".format(roles + 1, authenticatedRoles[roles].upper()))
        break

# Ask the User to Select the Operation to perform -
while True:
    operation = str(input("\nSelect the correct operation: "))
    if operation == '1':
        totalEmployees = int(input("Add the No. of Employees: "))
        # Add the total no. of Employees with Names
        print("\n")
        for emp in range(totalEmployees):
            while True:
                employeeName = input("\nEnter the {}. Employee Name: ".format(emp + 1)).upper()
                if employeeName in employeeList:
                    print(f'\nAn Employee with \"{employeeName}\" is ALREADY exist !'
                          f' Please Enter SEPARATE NAME for new employee.')
                else: employeeList.extend([employeeName]); break
        print(f"\nTotal {totalEmployees} Employees are CREATED")
        print("-------------------------------")
        for displayEmployee in range(len(employeeList)):
            print(f"{displayEmployee + 1}. {employeeList[displayEmployee]}")
        break
    else:
        print(f"Selected option \"{operation}\" is INCORRECT")
    print(f'\nSelect an Employee to SEARCH and ADD it to the Employee List if not existing')

# Searches an employee and return if existing and add to list if not
searchEmployee = str(input('\nEnter an Employee to Search from the List: ')).upper()
if searchEmployee in employeeList:
    print("\"{}\" is AVAILABLE in the Employee List".format(searchEmployee))
# else:
#     print("\n\"{}\" is NOT AVAILABLE in the Employee List".format(searchEmployee))
#     newEmployee = input(f'Would you like to add \"{searchEmployee}\" to Employee List (Yes / No) ?').upper()
#     if newEmployee == 'YES':
#         if len(employeeList == maxEmployeeList):
#             print(f'\nEmployee List already reached its MAXIMUM LENGTH'
#                   f'\n DELETE any Employee from teh List to add NEW EMPLOYEE')
#         print(f'\n\"{searchEmployee}\" is ADDED to Employee List')
#         employeeList.extend([searchEmployee]); employeeList.sort()
#         for displayEmployee in range(len(employeeList)):
#             print(f"{displayEmployee + 1}. {employeeList[displayEmployee]}")
#     else: print(f'\nSearched Employee \"{searchEmployee}\" NOT ADDED to the List')
